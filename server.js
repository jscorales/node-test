var http = require('http');
var express = require('express');
var app = express();

app.get('/', function(req, res) {
  res.status(200).json({ remoteIp: req.ip });
});

app.get('/test', function(req, res) {
  var options = {
      host: '108.35.21.132',
      port: 1725,
      path: '/',
      method: 'GET',
      headers: {
          accept: 'application/json'
      }
  };

  console.log("Start");
  var x = http.request(options,function(response){
      var str = '';
      response.on('data',function(data){
          console.log('readable');
          str += data;
      });
      response.on('end',function(){
          res.status(200).json(JSON.parse(str));
      });
  });

  x.end();
});

app.listen(3000);
console.log('Listening on port 3000...');